from sqlalchemy import ForeignKey, Table, Column, Integer, MetaData, Date
from ..auth.models import user

metadata = MetaData()

salary_table = Table(
    "salary",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("username", Integer, ForeignKey (user.c.id)),
    Column("date", Date, nullable=False),
    Column("sum", Integer, nullable=False),
)