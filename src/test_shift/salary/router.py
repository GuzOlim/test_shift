from datetime import datetime, date
from typing import Union
from fastapi import APIRouter, Depends
from pydantic import BaseModel
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi_users import fastapi_users, FastAPIUsers
from ..auth.database import get_async_session
from ..salary.models import salary_table
from ..auth.manager import get_user_manager
from ..auth.database import User
from ..auth.auth import auth_backend

router = APIRouter(
    prefix="/salary",
    tags=["Salary"]
)



fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)
current_user = fastapi_users.current_user()


class SalaryCurrent(BaseModel):
    id: int
    username: int
    sum: int

    class Config:
        orm_mode = True

class SalaryFuture(BaseModel):
    id: int
    username: int
    date: date

    class Config:
        orm_mode = True
    

@router.get("/current", response_model=Union[SalaryCurrent, None])
async def get_current_salary(user: User = Depends(current_user), session: AsyncSession = Depends(get_async_session)):
    query = select(salary_table.c.id, salary_table.c.username, salary_table.c.sum).where((salary_table.c.username == user.id) & (salary_table.c.date <= datetime.now().date())).order_by(salary_table.c.date.desc()) 
    result = await session.execute(query)
    return result.first()

@router.get("/future", response_model=Union[SalaryFuture, None])
async def get_salary_increase(user: User = Depends(current_user), session: AsyncSession = Depends(get_async_session)):
       query = select(salary_table.c.id, salary_table.c.username, salary_table.c.date).where((salary_table.c.username == user.id) & (salary_table.c.date > datetime.now().date())).order_by(salary_table.c.date)
       result = await session.execute(query) 
       return result.first()