import pytest
from sqlalchemy import insert, select

from .conftest import client, async_session_maker

def test_register():
    response = client.post("/auth/register", json={
        "email": "m4@mail.ru",
        "password": "1234",
        "is_active": True,
        "is_superuser": False,
        "is_verified": False,
        "username": "mm",
    })

    assert response.status_code == 201, "Запись не добавлена"
